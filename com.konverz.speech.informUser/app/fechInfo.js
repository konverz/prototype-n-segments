
var SERVER_URL = "https://3763d455.ngrok.io/dialogue/poll_replies"

var server_response = ""

/**
 * The callback to prepare a segment for play.
 * @param  {string} trigger The trigger type of a segment.
 * @param  {object} args    The input arguments.
 */
da.segment.onpreprocess = function (trigger, args) {
    console.log('onpreprocess', { trigger: trigger, args: args });
    if(args != null){
        server_response = getAPIData();
    }
    da.startSegment(null, null);
};

/**
 * The callback to start a segment.
 * @param  {string} trigger The trigger type of a segment.
 * @param  {object} args    The input arguments.
 */
da.segment.onstart = function (trigger, args) {
    console.log('onstart', { trigger: trigger, args: args });
    var synthesis = da.SpeechSynthesis.getInstance();
    synthesis.speak(server_response, {
        onstart: function () {
            console.log('speak start');
        },
        onend: function () {
            console.log('speak onend');
            da.stopSegment();
        },
        onerror: function (error) {
            console.log('speak cancel: ' + error.messsage);
            da.stopSegment();
        }
    });
};

var getAPIData = function () {
    var def = $.Deferred();

    //"userId": "u:8k8bzf  xijookk8ao", "text" : "<TEXT>"
    var payload = {
        user_id: "u:8k8bzfxijookk8ao",
        text: "What is in store for me now?" // a hard coded string, which will trigger the server to return tasks / reminders coming up shortly
    };

    var settings = {
            crossDomain: true,
            url: 'https://3763d455.ngrok.io/flock/listen',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(payload),
            contentType: "application/json",
            xhr: function () { return da.getXhr(); },
                success: function (data, textStatus, jqXHR) {
                    console.log('data request success');
                },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('request error');
            console.log('textStatus : ' + textStatus);
            console.log('errorThrown : ' + errorThrown);
            if (jqXHR !== null) {
                console.log('responseJSON : ' + jqXHR.responseJSON);
                console.log('status : ' + jqXHR.status);
            }
            if (textStatus === 'abort') {
                console.log('abort');
            }
            def.reject();
           }};
            $.ajax(settings);
            return def.promise();

}