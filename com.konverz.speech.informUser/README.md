informUser segment
=======================

Segment for looking up server for any of user's upcoming action items

Features
-------------
- It periodically queries the server, with the user's id.
- The server looks up the cache / db to check if the given user needs to be notified about anything
- The segment picks the response and Nigel speaks the same, if any

