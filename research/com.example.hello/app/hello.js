/*
 * Copyright 2016 Sony Corporation
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions, and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

/**
 * The callback to prepare a segment for play.
 * @param  {string} trigger The trigger type of a segment.
 * @param  {object} args    The input arguments.
 */
da.segment.onpreprocess = function (trigger, args) {
    console.log('onpreprocess', { trigger: trigger, args: args });
    da.startSegment(null, null);
};

/**
 * The callback to start a segment.
 * @param  {string} trigger The trigger type of a segment.
 * @param  {object} args    The input arguments.
 */
da.segment.onstart = function (trigger, args) {
    console.log('onstart', { trigger: trigger, args: args });
    var synthesis = da.SpeechSynthesis.getInstance();
    synthesis.speak('Hello World 2017 !', {
        onstart: function () {
            console.log('speak start');
        },
        onend: function () {
            console.log('speak onend');
            da.stopSegment();
        },
        onerror: function (error) {
            console.log('speak cancel: ' + error.messsage);
            da.stopSegment();
        }
    });
    var temp = getAPIData();
    temp.then(function(value){
        synthesis.speak(value);
    },
    function(error){
        console.log('speak cancel:' + error.message)
    });
};

// TEST FUNCTION
// var getAPIData = function () {
//     var def = $.Deferred();
//     var url = "https://api.ipify.org?format=json";
//     var setting = {
//         url: url,
//         type: 'GET',
//         dataType: 'json',
//         xhr: function () { return da.getXhr(); },
//         success: function (data, textStatus, jqXHR) {
//             console.log('data request success');
//             def.resolve(data);
//         },
//         error: function (jqXHR, textStatus, errorThrown) {
//             console.log('request error');
//             console.log('textStatus : ' + textStatus);
//             console.log('errorThrown : ' + errorThrown);
//             if (jqXHR !== null) {
//                 console.log('responseJSON : ' + jqXHR.responseJSON);
//                 console.log('status : ' + jqXHR.status);
//             }
//             if (textStatus === 'abort') {
//                 console.log('abort');
//             }
//             def.reject();
//         }
//     };
//     $.ajax(setting);
//     return def.promise();
// };

var getAPIData = function () {
    var def = $.Deferred();

    //"userId": "u:8k8bzf  xijookk8ao", "text" : "<TEXT>"
    var payload = {
        from: "u:8k8bzfxijookk8ao",
        text: "Whats my day like?"
    };

    var settings = {
            crossDomain: true,
            url: 'https://3763d455.ngrok.io/flock/listen',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(payload),
            contentType: "application/json",
            xhr: function () { return da.getXhr(); },
                success: function (data, textStatus, jqXHR) {
                    console.log('data request success');
                },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('request error');
            console.log('textStatus : ' + textStatus);
            console.log('errorThrown : ' + errorThrown);
            if (jqXHR !== null) {
                console.log('responseJSON : ' + jqXHR.responseJSON);
                console.log('status : ' + jqXHR.status);
            }
            if (textStatus === 'abort') {
                console.log('abort');
            }
            def.reject();
           }};
                    $.ajax(settings);
                    return def.promise();

}
