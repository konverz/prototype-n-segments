# Protype N Segments #

This repository houses the user interaction segments of Konverz, that are deployed on the Sony Protype N device.

It consists of the following segments:

* userQuery: Listens to user's input and provides appropiate response
* informUser: looks for any item that the user should be informed about.
