var SERVER_URL = 'https://3763d455.ngrok.io/dialogue/publish'
var speechText;

/**
 * The callback to prepare a segment for play.
 * @param  {string} trigger The trigger type of a segment.
 * @param  {object} args    The input arguments.
 */
da.segment.onpreprocess = function (trigger, args) {
    console.log('[SpeechToText] onpreprocess', {trigger: trigger, args: args});
    speechText = "";
    da.startSegment(null, null);
};

/**
 * The callback to resume a segment for play.
 */
da.segment.onresume = function () {
    console.log('onresume');
    var synthesis = da.SpeechSynthesis.getInstance();
    synthesis.speak('The result is ' + speechText, {
        onstart: function () {
            console.log('[SpeechToText] speak start');
        },
        onend: function () {
            console.log('[SpeechToText] speak onend');
            da.stopSegment();
        },
        onerror: function (error) {
            console.log('[SpeechToText] speak cancel: ' + error.message);
            da.stopSegment();
        }
    });

    if (speechText != "") {
        var entry = {
            domain: "Input Speech Text",
            extension: {},
            title: speechText,
            url: "https://translate.google.co.jp/?hl=ja#en/ja/" + speechText,
            imageUrl: "http://www.sony.net/SonyInfo/News/Press/201603/16-025E/img01.gif",
            date: new Date().toISOString()
        };
        da.addTimeline({entries: [entry]});
    }
};

/**
 * The callback to start a segment.
 * @param  {string} trigger The trigger type of a segment.
 * @param  {object} args    The input arguments.
 */
da.segment.onstart = function (trigger, args) {
    console.log('[SpeechToText] onstart', {trigger: trigger, args: args});
    var synthesis = da.SpeechSynthesis.getInstance();

    if (da.getApiLevel === undefined) {
        // API_LEVEL = 1;
        synthesis.speak('This device software is not available for speech to text function.', {
            onstart: function () {
                console.log('[SpeechToText] speak start');
            },
            onend: function () {
                da.stopSegment();
            },
            onerror: function (error) {
                da.stopSegment();
            }
        });
    } else {
        // // API_LEVEL = 2 or later;
        // synthesis.speak('Please say something.', {
        //     onstart: function () {
        //         console.log('[SpeechToText] speak start');
        //     },
        //     onend: function () {
        //         console.log('[SpeechToText] speak onend');

        //         var speechToText = new da.SpeechToText();
        //         speechToText.startSpeechToText(callbackobject);
        //     },
        //     onerror: function (error) {
        //         console.log('[SpeechToText] speak cancel: ' + error.message);
        //         da.stopSegment();
        //     }
        // });
        var speechToText = new da.SpeechToText();
        speechToText.startSpeechToText(callbackobject);

    }
};

var callbackobject = {
    onsuccess: function (results) {
        console.log('[SpeechToText] : SpeechToText process has finished successfully');
        console.log('[SpeechToText] : Results = ' + results);

        var strResults = results.join(" ");
        speechText = strResults;
        var response = getAPIData(speechText)
        response.then(function(value){
            synthesis.speak(value);
        },
        function(error){
            console.log('speak cancel:' + error.message)
        });

    },
    onerror: function (error) {
        console.log('[SpeechToText] : SpeechToText error message = ' + error.message)
        console.log('[SpeechToText] : SpeechToText error code = ' + error.code)

        var synthesis = da.SpeechSynthesis.getInstance();
        synthesis.speak('The speech to text API could not recognize what you said. Reason is ' + error.message, {
            onstart: function () {
                console.log('[SpeechToText] error message speak start');
            },
            onend: function () {
                console.log('[SpeechToText] error message speak onend');
                da.stopSegment();
            },
            onerror: function (error) {
                console.log('[SpeechToText] error message speak cancel: ' + error.message);
                da.stopSegment();
            }
        });
    }

};


var getAPIData = function (user_query) {
    var def = $.Deferred();

    //"userId": "u:8k8bzf  xijookk8ao", "text" : "<TEXT>"
    var payload = {
        from: "u:8k8bzfxijookk8ao",
        text: user_query
    };

    var settings = {
            crossDomain: true,
            url: SERVER_URL,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(payload),
            contentType: "application/json",
        xhr: function () { return da.getXhr(); },
        success: function (data, textStatus, jqXHR) {
                    console.log('data request success');
                },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log('request error');
            console.log('textStatus : ' + textStatus);
            console.log('errorThrown : ' + errorThrown);
            if (jqXHR !== null) {
                console.log('responseJSON : ' + jqXHR.responseJSON);
                console.log('status : ' + jqXHR.status);
            }
            if (textStatus === 'abort') {
                console.log('abort');
            }
            def.reject();
           }};

    $.ajax(settings);
    return def.promise();

}