userQuery segment
=======================

Segment for listening to user's queries and responding to them.

Features
-------------
- It catches anything that the user address's to Nigel
- The user query is sent to the server. The query is processed using NLP alogirthms to understand the meaning of the query. The server performs the requesite actions and returns the response that Nigel should give.
- The segment picks the response and Nigel speaks the same

